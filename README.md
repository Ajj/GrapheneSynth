# GrapheneSynth - Currently Windows Only

####  1. Install gRPC dependencies

* see [here](https://github.com/grpc/grpc/blob/master/BUILDING.md)

#### 2. Clone this repository
`git clone --recurse-submodules https://gitlab.com/teamaxe/GrapheneSynth.git`

#### 3. Build the NarupaCppClient project
* create a build folder in `narupacppclient` and run cmake (your VS version or architecture may vary):
```
mkdir narupacppclient\build
cd narupacppclient\build
cmake -G "Visual Studio 16 2019" -A x64 ..
```
The last command will take some time as it downloads some dependencies

#### 4. Building the GrapheneSynth Audio Plugin

* Open `GrapheneSynth.jucer` in the Projucer - if you don't have JUCE installed then get it [here](https://github.com/WeAreROLI/JUCE) - build and run the Projucer (located in the Juce `extras` folder)

* Save and open the GrapheneSynth visual studio solution
* Add the narupaclientlib as an additional solution:
* In visual studio right click the GrapheneSynth solution, add -> existing project
	- in the dropdown menu select "Solution Files (*.sln)"
	- navigate to and select `\narupacppclient\build\NarupaClient.sln` (this may take a minute)
* For the `GrapheneSynth_SharedCode`, `GrapheneSynth_StandalonePlugin` and `GrapheneSynth_UnityPlugin` targets, right click references -> add references and tick:
	- `NarupaClientLib`

Cross those fingers and build

## Running the simulation 

Make sure [narupa-protocol](https://gitlab.com/intangiblerealities/narupa-protocol) is installed 

```
conda create -n narupa "python>3.6"
conda activate narupa
conda install -c irl -c omnia -c conda-forge narupa-server
conda install jupyter
```

Run the notebook:

```
cd graphenesynth/GrapheneSimulation
jupyter notebook
```

Click on `graphene_synthesis.ipynb` and run all the cells to start the simulation
and set up some sliders to control the parameters. 
