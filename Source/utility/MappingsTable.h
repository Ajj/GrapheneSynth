/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Mappings.h"

class MappingSelectionMenu : public Component
{
public:
    enum paramsEnumBox
    {
        mappingDirection = 0, 
        simParameter, 
        midiChannel, 
        midiType,  
        polarity,
        numBoxParams
    };
    enum paramsEnumEditor
    {
        midiNumber,
        centre,
        range,
        numEditorParams
    };

    MappingSelectionMenu();
    ~MappingSelectionMenu();

    void paint(Graphics&) override;
    void resized() override;

    Mapping getCurrentState();
    
private:
    StringArray comboBoxParameterNames = StringArray("mappingDirection", "simParameter", "midiChannel", "midiType", "polarity");
    StringArray textEditorParameterNames = StringArray("midiNumber", "centre", "range");

    OwnedArray<ComboBox> parameterBoxes;
    OwnedArray<Label> parameterLabels;
    OwnedArray<TextEditor> parameterTextBoxes;
};

class MappingsTable : public Component, public TableListBoxModel, public Button::Listener
{
public:
	MappingsTable(Mappings& m);
	~MappingsTable();

    void loadData(XmlElement& data);
    void reloadData(XmlElement& data);

    void buttonClicked(Button* b) override;

    void update();
    void modifyMapping(int rowNumber, int columnNumber);

    String getText(const int columnNumber, const int rowNumber) const
    {
        return dataList->getChildElement(rowNumber)->getStringAttribute(getAttributeNameForColumnId(columnNumber));
    }

    void setText(const int columnNumber, const int rowNumber, const String& newText)
    {
        const auto& columnName = table.getHeader().getColumnName(columnNumber);
        dataList->getChildElement(rowNumber)->setAttribute(columnName, newText);
    }

    int getSelection(const int rowNumber) const
    {
        return dataList->getChildElement(rowNumber)->getIntAttribute("Select");
    }

    void setSelection(const int rowNumber, const int newSelection)
    {
        dataList->getChildElement(rowNumber)->setAttribute("Select", newSelection);
    }

    String getAttributeNameForColumnId(const int columnId) const
    {
        forEachXmlChildElement(*columnList, columnXml)
        {
            if (columnXml->getIntAttribute("columnId") == columnId)
            {
                return columnXml->getStringAttribute("name");
            }
        }

        return {};
    }
	//==============================================================================
	void paint(Graphics&) override;
	void resized() override;

    //==============================================================================

    int getNumRows() override;

    void paintRowBackground(Graphics&, int rowNumber, int width, int height, bool rowIsSelected) override;

    void paintCell(Graphics&, int rowNumber, int columnId, int width, int height, bool rowIsSelected) override;

    void sortOrderChanged(int newSortColumnId, bool isForwards) override;

    Component* refreshComponentForCell(int rowNumber, int columnId, bool isRowSelected, Component* existingComponentToUpdate) override;

    int getColumnAutoSizeWidth(int columnId) override;

    void selectedRowsChanged(int lastRowSelected) override;

private:    
	Mappings& mappings;

	TableListBox table{ {}, this };

    std::atomic<int> selectedRow = 0;

    std::unique_ptr <XmlElement> mappingsData;
    std::unique_ptr <XmlElement> columnList = nullptr;
    std::unique_ptr <XmlElement> dataList = nullptr;

	int numRows;
    Font font{ 14.0f };

    TextButton createMappingButton;
    TextButton removeMappingButton;
    MappingSelectionMenu mappingSelectionMenu;

class EditableTextCustomComponent : public Label
{
public:
    EditableTextCustomComponent(MappingsTable& td)
        : owner(td)
    {
        setEditable(false, true, false);
    }
    
    void mouseDown(const MouseEvent& event) override
    {
        owner.table.selectRowsBasedOnModifierKeys(row, event.mods, false);

        Label::mouseDown(event);
    }

    void textWasEdited() override
    {
        owner.setText(columnId, row, getText());
        owner.modifyMapping(row, columnId);
        owner.update();
    }

    void setRowAndColumn(const int newRow, const int newColumn)
    {
        row = newRow;
        columnId = newColumn;
        setText(owner.getText(columnId, row), dontSendNotification);
    }

private:
    MappingsTable& owner;
    int row, columnId;
    Colour textColour;
};

class SelectionColumnCustomComponent : public Component
{
public:
    SelectionColumnCustomComponent(MappingsTable& td)
        : owner(td)
    {
        addAndMakeVisible(toggleButton);

        toggleButton.onClick = [this] { owner.setSelection(row, (int)toggleButton.getToggleState()); };
    }

    void resized() override
    {
        toggleButton.setBoundsInset(BorderSize<int>(2));
    }

    void setRowAndColumn(int newRow, int newColumn)
    {
        row = newRow;
        columnId = newColumn;
        toggleButton.setToggleState((bool)owner.getSelection(row), dontSendNotification);
    }

private:
    MappingsTable& owner;
    ToggleButton toggleButton;
    int row, columnId;
};

class MappingsDataSorter
{
public:
    MappingsDataSorter(const String& attributeToSortBy, bool forwards)
        : attributeToSort(attributeToSortBy),
        direction(forwards ? 1 : -1)
    {}

    int compareElements(XmlElement* first, XmlElement* second) const
    {
        auto result = first->getStringAttribute(attributeToSort)
            .compareNatural(second->getStringAttribute(attributeToSort)); 

        if (result == 0)
            result = first->getStringAttribute("ID")
            .compareNatural(second->getStringAttribute("ID"));             

        return direction * result;                                                          
    }

private:
    String attributeToSort;
    int direction;
};
};