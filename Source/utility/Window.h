/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once
#include <vector>
#include "Histogram.h" 
#include <utility>
#include <functional>
#include "../JuceLibraryCode/JuceHeader.h"

static Identifier fileLogType ("fileLog");
static Identifier frameType ("frame");
static Identifier frameNumberType ("frameNumber");
static Identifier minType ("min");
static Identifier maxType ("max");
static Identifier currentType ("currentValue");
static Identifier meanType ("mean");
static Identifier sdType ("sd");
static Identifier nvType ("normalisedValue");

class Window
{
public:
    struct dataStruct
    {
        Atomic<int> frameNumber;
        Atomic<float> min;
        Atomic<float> max;
        Atomic<float> nextValue;
        Atomic<float> mean;
        Atomic<float> standardDeviation;
        Atomic<float> normalisedValue;
    };
    Window ();
    ~Window();

    float getNormalisedValue (float nextValue);

    std::vector<dataStruct>& getData() { return data; }
private:

    //Window() {}
    Histogram histogram;
    std::vector<float> buffer;
    bool go                         = false;
    const int bufferSize            = 50;
    int bufferWritePos              = 0;
    float standardDeviation         = 0.f;
    float mean                      = 0.f;
    float min                       = 0.f;
    float max                       = 0.f;
   
    std::vector<float> distribution;
    std::vector<dataStruct> data;
    dataStruct test;
    int frameNumber = 0;
    ValueTree fileLog;
};
