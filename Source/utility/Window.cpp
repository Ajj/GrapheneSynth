/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "Window.h"
#include <numeric>

Window::Window() : fileLog(fileLogType)
{
    buffer.resize(bufferSize);  
}

Window::~Window()
{

}

float Window::getNormalisedValue(float nextValue)
{
    if (isnan<float>(nextValue) || nextValue == 0)
        return 0.0;

    // insert the new value into the buffer at the write position
    std::vector<float>::iterator it = buffer.begin() + bufferWritePos;
    buffer.insert(it, nextValue);

    // increment the write position
    bufferWritePos++;

    auto normalisedValue = 0.0;

    // if we've reached the end of the buffer (the buffer is now full)
    if (bufferWritePos > bufferSize - 1)
    {
        // reset the write position
        bufferWritePos = 0;
        // create a histogram of the current buffer
        distribution = histogram.getBuckets(buffer, 20);
        mean = 0.0f;
        standardDeviation = 0.0f;

        auto size = (float)buffer.size();

        if (size > 0)
        {
            // calculate the mean value of the buffer
            mean = std::accumulate(buffer.begin(), buffer.end(), 0.f) / size;

            //auto sum = std::accumulate(buffer.begin(), buffer.end(), 0.f);
            //mean = sum / size;

            auto sq_sum = std::inner_product(buffer.begin(), buffer.end(), buffer.begin(), 0.f);
            standardDeviation = std::sqrtf(sq_sum / size - mean * mean);
        }

        min = mean - (2.0f * standardDeviation);
        max = mean + (2.0f * standardDeviation);

        go = true;
    }

    if (go)
        normalisedValue = (nextValue - min) / (max - min);

    std::clamp<float>(normalisedValue, 0, 1);
 
    dataStruct* d = new dataStruct;

    d->frameNumber = frameNumber++;
    d->max = max;
    d->min = min;
    d->mean = mean;
    d->standardDeviation = standardDeviation;
    d->normalisedValue = normalisedValue;
    d->nextValue = nextValue;

    data.push_back(*d);
    //DBG("data has size " << data.size());
    return normalisedValue;
}
