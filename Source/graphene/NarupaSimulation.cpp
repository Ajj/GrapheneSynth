/* 
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "NarupaSimulation.h"

const std::vector<int> NarupaPath::getPath (PathType pathType)
{
    //must be in this range
    switch (pathType)
    {
        case PathType::SpiralNanoTube:        return { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59 };
        case PathType::StringAlanine:        return { 1, 2, 132, 133, 134, 12, 13, 142, 143, 144, 14, 15, 22, 23, 152, 153, 154, 24, 32, 33, 162, 163, 34, 38, 42, 43, 44, 52, 53, 54, 62, 63, 64, 72, 73, 74, 82, 83, 84, 92, 93, 94, 102, 104, 112, 113, 114, 122, 123, 124 };
        case PathType::Hex5:                return { 541, 540, 542, 503, 505, 504, 506, 467, 469, 468, 429, 428, 389, 388, 349, 348, 309, 308, 269, 267, 266, 264, 225, 223, 222, 220, 181, 179, 218, 216, 217, 215, 254, 252, 253, 251, 290, 291, 330, 331, 370, 371, 410, 411, 450, 451, 453, 492, 494, 495, 497, 536, 538, 539 };
        case PathType::LineDiag1:           return { 521, 522, 523, 268, 269, 270, 271, 412, 413, 414, 415, 304, 305, 306, 307, 448, 449, 450, 451, 196, 197, 198, 199, 340, 341, 342, 343, 484, 485, 486, 487, 232, 233, 234, 235, 376, 377, 378, 379 };
    }
}

NarupaSimulation::NarupaSimulation() : Thread ("InteractionEventThread")
{
    trajectoryService.setPositionCallback ([this](std::vector<float> newPositions)
    {
        {
            std::lock_guard<std::mutex> lg(positionsLock);
            positions = newPositions;
        }
        std::lock_guard<std::mutex> lg(listenerLock);
        listeners.call(&Listener::simulationUpdate, newPositions);
    });
    startThread();
}

NarupaSimulation::~NarupaSimulation()
{
    stopThread (1000);
}

int NarupaSimulation::getParticleCount()
{
    std::lock_guard<std::mutex> lg (positionsLock);
    return positions.size() / 3;
}

void NarupaSimulation::pluck(float scale, int durationMs)
{
    int64 currentTime = Time::currentTimeMillis();
    const std::string id = std::to_string(idGenerator.get());

    if (molecule == Alanine)
    {
        ImdService::Interaction interaction(id, { 0.f, 0.f, 0.f }, 82, scale, ImdService::Interaction::ForceType::Spring);
        addEvent(currentTime, interaction);
        addEvent(currentTime + durationMs, interaction, true);
    }
    else if (molecule == Nanotube)
    {
        ImdService::Interaction interaction(id, { 0.f, 0.f, 0.f }, 55, scale, ImdService::Interaction::ForceType::Spring);
        addEvent(currentTime, interaction);
        addEvent(currentTime + durationMs, interaction, true);
    }
    else if (molecule == Graphene)
    {
        ImdService::Interaction interaction(id, { 0.f, 0.f, 0.f }, 468, scale, ImdService::Interaction::ForceType::Spring);
        addEvent(currentTime, interaction);
        addEvent(currentTime + durationMs, interaction, true);
    }
}

void NarupaSimulation::getPositionsCopy (std::vector<float>& containerForPositionsCopy)
{
    std::lock_guard<std::mutex> lg (positionsLock);
    containerForPositionsCopy = positions;
}

void NarupaSimulation::addListener (NarupaSimulation::Listener* listenerToAdd)
{
    std::lock_guard<std::mutex> lg (listenerLock);
    listeners.add (listenerToAdd);
}

void NarupaSimulation::removeListener (NarupaSimulation::Listener* listenerToRemove)
{
    std::lock_guard<std::mutex> lg (listenerLock);
    listeners.remove (listenerToRemove);
}

void NarupaSimulation::run()
{
    while (! threadShouldExit())
    {
        int waitTime = 100;
        {
            std::lock_guard<std::mutex> lg (eventListLock);

            for (auto iter = eventList.begin(); iter != eventList.end(); )
            {
                auto eventTime = iter->time;
                auto currentTime = Time::currentTimeMillis();

                if (currentTime >= eventTime)
                {
                    if (iter->endStream)
                        imdService.endInteraction (iter->interaction.id);
                    else
                        imdService.beginOrUpdateInteration (iter->interaction);

                    iter = eventList.erase (iter);
                }
                else
                {
                    waitTime = eventTime - currentTime;
                    ++iter;
                    break; //leave subsequent events
                }
            }
        }
        wait (waitTime);
    }
}
