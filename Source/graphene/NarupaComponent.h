/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "NarupaSynthesiser.h"
#include <array>
#include "../utility/MappingsTable.h"

class NarupaVisualisation :   public	Component,
                                private Timer
{
public:
    NarupaVisualisation (NarupaSynthesiser& gs);
    ~NarupaVisualisation();

    //Component
    void paint(Graphics&) override;

private:
    NarupaVisualisation() = default;
    enum class Axis
    {
    	X,
    	Y,
    	Z
    };

    int coordinateToPixel (Axis axis, float value) const;
    void timerCallback() override;

    NarupaSynthesiser& narupaSynthesiser;

    std::array<Range<float>, 3> ranges;
    std::vector<float> positions;
};

class NarupaControls : public	Component
{
public:
    NarupaControls (NarupaSimulation& gs);
    ~NarupaControls();

    //Component
    void resized() override;
    void paint(Graphics& g) override;
private:
    NarupaControls() = default;
    NarupaSimulation& narupaSimulation;
    Slider temperatureSlider {"Temperature "};
    Slider frictionSlider { "Friction " };
    Slider timestepSlider { "Timestep " };
    Label temperatureLabel{"Temperature", "Temperature" };
    Label frictionLabel{ "Friction", "Friction" };
    Label timestepLabel{ "TimeStep", "TimeStep" };
      
    TextButton pluckButton {"Pluck"};
};

class AdsrControls : public	Component
{
public:
    AdsrControls(NarupaSynthesiser& gs);
    ~AdsrControls();
    void resized() override;
    void paint(Graphics& g) override;

private:
    AdsrControls() = default;
    NarupaSynthesiser& narupaSynth;
    enum ParamName                      { Attack ,  Decay ,  Sustain ,  Release, NumParameters };
    const StringArray ParameterNames    {"Attack", "Decay", "Sustain", "Release"};
    const StringArray ShortNames        {  "A"   ,   "D"  ,    "S"   ,    "R"   };
    std::vector<std::unique_ptr<Slider>> paramSliders;
    std::vector <std::unique_ptr<Label>> paramLabels;
};

class WavetableControls : public Component
{
public:
    WavetableControls(NarupaSynthesiser& gs);
    ~WavetableControls() {}

    void resized() override;
    void paint(Graphics& g) override;
private:
    Slider FilterCoefficientSlider{"Filter Coefficient Slider"};
    Label FIlterCoefficientLabel  {"Smoothing Factor", "Smoothing Factor" };
    NarupaSynthesiser& gs;
};

class WavetableComponent :	public Component,
							private Timer
{
public:
    WavetableComponent (NarupaSynthesiser& gs);
    ~WavetableComponent();

    //Component
    void paint(Graphics&) override;
private:
    WavetableComponent() = default;
    void timerCallback() override;

    NarupaSynthesiser& narupaSynthesiser;
    std::vector<float> wavetable;
};


class NarupaComponent : public Component, public Button::Listener
{
public:
    NarupaComponent (NarupaSynthesiser& gs);
    ~NarupaComponent();

    //Component
    void resized() override;

    void buttonClicked(Button* b) override;

private:
    NarupaComponent() = default;
    NarupaVisualisation narupaVisualisation;
    NarupaControls narupaControls;
    WavetableComponent wavetableComponent;
    AdsrControls adsrControls;
    WavetableControls wavetableControls;
    MappingsTable mappingsTable;

    ToggleButton mappingTableStateButton;
    Label mappingTableLabel;
};