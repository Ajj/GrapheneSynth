/*
 * Copyright (c) 2020 Tom Mitchell and Alex Jones.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"
#include "NarupaSimulation.h"
#include <array>
#include "../utility/Mappings.h"

enum WavetableConstants { NumWavetables = 5, OversampleAmount = 8 };

enum ScanPathMethod {DistanceToCentroid, FilteredVelocity};

enum SimulationParmIndices { temperatureID, frictionID, timestepID };

//==============================================================================
/** Our demo synth voice just plays a sine wave.. */
class WavetableVoice : public SynthesiserVoice
{
public:
    WavetableVoice() = delete;
    WavetableVoice (std::array<std::vector<float>, NumWavetables>& w, std::atomic<int>& wi, int sps);
    void setFrequency (float newFrequency);
    float getNextSample();
    void getNextFrame(float* frame, int frameSize);
    void setExpectedBlockSize(int blockSize);

    bool canPlaySound (SynthesiserSound* sound) override;
    void startNote (int midiNoteNumber, float velocity, SynthesiserSound*, int /*currentPitchWheelPosition*/) override;
    void stopNote (float /*velocity*/, bool allowTailOff) override;
    void pitchWheelMoved (int /*newValue*/) override {}
    void controllerMoved (int /*controllerNumber*/, int /*newValue*/) override {}
    void setCurrentPlaybackSampleRate (double newRate) override;
    void renderNextBlock (AudioBuffer<float>& outputBuffer, int startSample, int numSamples) override;

    void setAttack  (double newAttack );
    void setDecay   (double newDecay  );
    void setSustain (double newSustain);
    void setRelease (double newRelease);
    
    using SynthesiserVoice::renderNextBlock;
private:
    double level = 0.0;
    int scanPathSize        { 0 };
    float elementsPerSample { 0.f };
    float elementPosition   { 0.f };

    std::array<std::vector<float>, NumWavetables>& wavetables;
    std::atomic<int>& wavetableIndex;
    int currentWavetable { 0 };
    int nextWavetable { 0 };    //wavetable to fade to next
    float wavefade { 1.1f };
    float wavefadeRate {1.f / 240.f}; //fade time 5 ms at 48 kHz
    ADSR envelope;
    ADSR::Parameters envelopeParams;
    std::atomic<float> attack   {0.1f};
    std::atomic<float> decay    {0.2f};
    std::atomic<float> sustain  {0.9f};
    std::atomic<float> release  {0.5f};

    class DcBlocker
    {
    public:
        DcBlocker() = default;
        float processSample (float x)
        {
            auto y = x - xm1 + 0.995f * ym1;
            xm1 = x;
            ym1 = y;
            return y;
        }
        void processFrame (float* frame, int frameSize)
        {
            for (auto i = 0; i < frameSize; i++)
            {
                auto x = frame[i];
                auto y = x - xm1 + 0.995f * ym1;
                xm1 = x;
                ym1 = y;
                frame[i] = y;
            }
        }
    private:
        float xm1{ 0.f };
        float ym1{ 0.f };
    };
    DcBlocker dcBlocker;
    LagrangeInterpolator lagrangeInterpolator; //for audiosample buffer downsampling
};

class NarupaSynthesiser : private NarupaSimulation::Listener, public AudioProcessorListener, public HighResolutionTimer
{
public:
    NarupaSynthesiser();
    ~NarupaSynthesiser();

    NarupaSimulation& getNarupaSimulation() { return narupaSimulation; }
    Mappings& getMappings() { return mappings; }

    void getPositionsCopy (std::vector<float>& containerForPositionsCopy);
    void getWavetableCopy (std::vector<float>& containerForWavetableCopy);
    const std::array<float, 3>& getCentroid() const;

    void prepareToPlay (double newSampleRate, int maximumExpectedSamplesPerBlock);
    void releaseResources();
    void processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages);

    void setAttack (double newAttack);
    void setDecay (double newDecay);
    void setSustain (double newSustain);
    void setRelease (double newRelease);

    void setFilterCoefficient(float newCoefficient);

    void audioProcessorParameterChanged(AudioProcessor* processor, int parameterIndex, float newValue) override;
    void audioProcessorChanged(AudioProcessor * processor) override;

    void hiResTimerCallback() override;

    std::vector<NormalisableRange<float>> getSimulationParameterRanges() const { return simulationParamRanges; }
private:
    void calculateCentroid();
    void calculateInitialScanPathDistances();

    void initialiseFilters();

    void simulationUpdate (const std::vector<float> newPositions) override;

    //======================================================================
    //const int scanPathMethod = DistanceToCentroid;
    const int scanPathMethod = FilteredVelocity;

    std::vector<NormalisableRange<float>> simulationParamRanges;

    class OnePoleFilter
    {
    public:
        OnePoleFilter() = default;
        void setId (int id_) { id = id_; }
        int getId() { return id; }
        void setTargetValue (float newValue)
        {
            previousValue = newValue;
        }
        void init (float coeff)
        {
            a = coeff;
            b = 1.0f - coeff;
        }
        float getNextSample (float input) 
        { 
            return previousValue = (a * input) + (b * previousValue);
        }
    private:
        float a             { 0.0f };
        float b             { 0.0f };
        float previousValue { 0.0f };
        int id              { 0 };
    };

    std::vector<OnePoleFilter> filters;
    //======================================================================

    NarupaSimulation narupaSimulation;
    float sampleRate { 44100.f };
    int polyphony { 16 };

    std::vector<float> initialPositions;
    std::vector<int> scanPathIndices;
    std::vector<float> scanPathInitialCentroidDistances;
    std::array<float, 3> centroid { 0.f, 0.f, 0.f };

    std::array<std::vector<float>, NumWavetables> wavetables;
    std::atomic<int> wavetableIndex { 0 };
    std::atomic<bool> wavetablesReady { false };

    std::atomic<float> temperature;
    std::atomic<float> friction;
    std::atomic<float> timestep;

    std::mutex wavetableForPlotLock;
    std::vector<float> wavetableForPlot;

    Synthesiser synth;

    Mappings mappings;

    bool isSimulationConnected = false;

    Array<int> particlesToRestrain{ 0, 4, 24, 25, 26, 28, 36, 37, 38, 40, 56, 57, 58, 60, 70, 71, 72, 74, 84, 85, 86, 88, 103, 104, 105, 107, 117, 118, 119, 121, 139, 140, 141, 143, 146, 147, 148, 150, 165, 166, 167, 169, 175, 176, 177, 179, 189, 190, 191, 193, 208, 209, 210, 212, 222, 223, 224, 226, 233, 234, 235, 237, 257, 258, 259, 261, 274, 275, 276, 278, 293, 294, 295, 297, 314, 315, 316, 318, 321, 322, 323, 325, 343, 344, 345, 347, 355, 356, 357, 359, 369, 370, 371, 373, 379, 380, 381, 383, 395, 396, 397, 399, 419, 420, 421, 423, 438, 439, 440, 442, 445, 446, 447, 449, 460, 461, 462, 464, 472, 473, 474, 476, 483, 484, 485, 487, 495, 496, 497, 499, 511, 512, 513, 515, 530, 531, 532, 534, 546, 547, 548, 550, 560, 561, 562, 564, 584, 585, 586, 588, 599, 600, 601, 611, 613, 614, 615, 617, 634, 635, 636, 638, 650, 651, 652, 654, 661, 662, 663, 665, 671, 672, 673, 675, 683, 684, 685, 695, 697, 698, 699, 701, 709, 710, 711, 713, 724, 725, 726, 728, 734, 735, 736, 738, 758, 759, 760, 762, 778, 779, 780, 782, 799, 800, 801, 803, 809, 810, 811, 813, 828, 829, 830, 832, 839, 840, 841, 843, 856, 857, 858, 860, 863, 864, 865, 867, 877, 878, 879, 881, 891, 892, 893, 895, 910, 911, 912, 914, 934, 935, 936, 938, 941, 942, 943, 945, 963, 964, 965, 967, 980, 981, 982, 984, 991, 992, 993, 995, 1005, 1006, 1007, 1009, 1012, 1013, 1014, 1016, 1026, 1027, 1028, 1030, 1045, 1046, 1047, 1049, 1062, 1063, 1064, 1066, 1074, 1075, 1076, 1078, 1098, 1099, 1100, 1102, 1109, 1110, 1111, 1113, 1126, 1127, 1128, 1130, 1147, 1148, 1149, 1151, 1171, 1172, 1173, 1175, 1181, 1182, 1183, 1185, 1200, 1201, 1202, 1204, 1219, 1220, 1221, 1223, 1230, 1231, 1232, 1234, 1254, 1255, 1256, 1266, 1268, 1269, 1270, 1272, 1287, 1288, 1289, 1291, 1298, 1299, 1300, 1302, 1309, 1310, 1311, 1321, 1323, 1324, 1325, 1335, 1337, 1338, 1339, 1341, 1351, 1352, 1353, 1355, 1367, 1368, 1369, 1371, 1388, 1389, 1390, 1392, 1402, 1403, 1404, 1406, 1413, 1414, 1415, 1417, 1437, 1438, 1439, 1441, 1453, 1454, 1455, 1457, 1468, 1469, 1470, 1472, 1478, 1479, 1480, 1482, 1497, 1498, 1499, 1501, 1504, 1505, 1506, 1508, 1528, 1529, 1530, 1532, 1539, 1540, 1541, 1543, 1550, 1551, 1552, 1554, 1564, 1565, 1566, 1568, 1575, 1576, 1577, 1579, 1585, 1586, 1587, 1589, 1602, 1603, 1604, 1606, 1614, 1615, 1616, 1618, 1621, 1622, 1623, 1625, 1643, 1644, 1645, 1647, 1657, 1658, 1659, 1661, 1681, 1682, 1683, 1685, 1698, 1699, 1700, 1702, 1709, 1710, 1711, 1713, 1728, 1729, 1730, 1732, 1738, 1739, 1740, 1742, 1757, 1758, 1759, 1761, 1768, 1769, 1770, 1772, 1775, 1776, 1777, 1787, 1789, 1790, 1791, 1793, 1803, 1804, 1805, 1807, 1817, 1818, 1819, 1821, 1831, 1832, 1833, 1835, 1841, 1842, 1843, 1845, 1852, 1853, 1854, 1856, 1862, 1863, 1864, 1866, 1878, 1879, 1880, 1882, 1897, 1898, 1899, 1901, 1921, 1922, 1923, 1925, 1942, 1943, 1944, 1946, 1956, 1957, 1958, 1960, 1980, 1981, 1982, 1984, 2004, 2005, 2006, 2016, 2018, 2019, 2020, 2022, 2034, 2035, 2036, 2038, 2048, 2049, 2050, 2052, 2063, 2064, 2065, 2067, 2082, 2083, 2084, 2086, 2096, 2097, 2098, 2100, 2110, 2111, 2112, 2114, 2134, 2135, 2136, 2138, 2144, 2145, 2146, 2148, 2168, 2169, 2170, 2172, 2182, 2183, 2184, 2186, 2201, 2202, 2203, 2205, 2220, 2221, 2222, 2224, 2244, 2245, 2246, 2248, 2258, 2259, 2260, 2262, 2275, 2276, 2277, 2279, 2290, 2291, 2292, 2294, 2301, 2302, 2303, 2305, 2316, 2317, 2318, 2320, 2326, 2327, 2328, 2330, 2342, 2343, 2344, 2346, 2352, 2353, 2354, 2356, 2369, 2370, 2371, 2373, 2383, 2384, 2385, 2387, 2390, 2391, 2392, 2394, 2406, 2407, 2408, 2410, 2416, 2417, 2418, 2428, 2430, 2431, 2432, 2434, 2446, 2447, 2448, 2450, 2462, 2463, 2464, 2466, 2482, 2483, 2484, 2486, 2496, 2497, 2498, 2500, 2508, 2509, 2510, 2512, 2515, 2516, 2517, 2519, 2526, 2527, 2528, 2530, 2536, 2537, 2538, 2540, 2550, 2551, 2552, 2554, 2557, 2558, 2559, 2569, 2571, 2572, 2573, 2575, 2581, 2582, 2583, 2585, 2596, 2597, 2598, 2600, 2610, 2611, 2612, 2614, 2634, 2635, 2636, 2638, 2653, 2654, 2655, 2657, 2674, 2675, 2676, 2678, 2695, 2696, 2697, 2699, 2715, 2716, 2717, 2719, 2737, 2738, 2739, 2741, 2752, 2753, 2754, 2756, 2759, 2760, 2761, 2763, 2781, 2782, 2783, 2785, 2800, 2801, 2802, 2804, 2819, 2820, 2821, 2823, 2841, 2842, 2843, 2845, 2865, 2866, 2867, 2869, 2880, 2881, 2882, 2892, 2894, 2895, 2896, 2898, 2913, 2914, 2915, 2917, 2923, 2924, 2925, 2927, 2930, 2931, 2932, 2934, 2944, 2945, 2946, 2948, 2954, 2955, 2956, 2958, 2976, 2977, 2978, 2980, 2993, 2994, 2995, 2997, 3012, 3013, 3014, 3016, 3027, 3028, 3029, 3031, 3042, 3043, 3044, 3046, 3052, 3053, 3054, 3056, 3063, 3064, 3065, 3067, 3073, 3074, 3075, 3077, 3094, 3095, 3096, 3098, 3101, 3102, 3103, 3105, 3116, 3117, 3118, 3120, 3140, 3141, 3142, 3144, 3150, 3151, 3152, 3154, 3165, 3166, 3167, 3169, 3184, 3185, 3186, 3188, 3198, 3199, 3200, 3202, 3208, 3209, 3210, 3212, 3222, 3223, 3224, 3226, 3232, 3233, 3234, 3236, 3256, 3257, 3258, 3260, 3268, 3269, 3270, 3272, 3282, 3283, 3284, 3286, 3306, 3307, 3308, 3310, 3323, 3324, 3325, 3327, 3330, 3331, 3332, 3334, 3341, 3342, 3343, 3345, 3355, 3356, 3357, 3359, 3379, 3380, 3381, 3391, 3393, 3394, 3395, 3397, 3409, 3410, 3411, 3413, 3428, 3429, 3430, 3432, 3452, 3453, 3454, 3456, 3471, 3472, 3473, 3475, 3483, 3484, 3485, 3495, 3497, 3498, 3499, 3501, 3513, 3514, 3515, 3517, 3523, 3524, 3525, 3527, 3540, 3541, 3542, 3544, 3554, 3555, 3556, 3558, 3571, 3572, 3573, 3575, 3585, 3586, 3587, 3589, 3596, 3597, 3598, 3600, 3613, 3614, 3615, 3617, 3634, 3635, 3636, 3638, 3653, 3654, 3655, 3657, 3663, 3664, 3665, 3667, 3674, 3675, 3676, 3686, 3688, 3689, 3690, 3692, 3704, 3705, 3706, 3708, 3723, 3724, 3725, 3727, 3737, 3738, 3739, 3741, 3749, 3750, 3751, 3753, 3763, 3764, 3765, 3775, 3777, 3778, 3779, 3781, 3801, 3802, 3803, 3813, 3815, 3816, 3817, 3819, 3829, 3830, 3831, 3833, 3841, 3842, 3843, 3853, 3855, 3856, 3857, 3859, 3869, 3870, 3871, 3873, 3885, 3886, 3887, 3889, 3892, 3893, 3894, 3896, 3914, 3915, 3916, 3918, 3924, 3925, 3926, 3928, 3938, 3939, 3940, 3942, 3950, 3951, 3952, 3962, 3964, 3965, 3966, 3968, 3985, 3986, 3987, 3997, 3999, 4000, 4001, 4003, 4006, 4007, 4008, 4010, 4020, 4021, 4022, 4024, 4034, 4035, 4036, 4038, 4048, 4049, 4050, 4052, 4062, 4063, 4064, 4066, 4069, 4070, 4071, 4073, 4085, 4086, 4087, 4089, 4107, 4108, 4109, 4111, 4114, 4115, 4116, 4118, 4134, 4135, 4136, 4138, 4145, 4146, 4147, 4149, 4166, 4167, 4168, 4170, 4185, 4186, 4187, 4189, 4197, 4198, 4199, 4201, 4204, 4205, 4206, 4208, 4220, 4221, 4222, 4224, 4234, 4235, 4236, 4238, 4248, 4249, 4250, 4252, 4272, 4273, 4274, 4276, 4291, 4292, 4293, 4295, 4298, 4299, 4300, 4302, 4322, 4323, 4324, 4326, 4336, 4337, 4338, 4340, 4355, 4356, 4357, 4359, 4366, 4367, 4368, 4370, 4385, 4386, 4387, 4389, 4395, 4396, 4397, 4399, 4406, 4407, 4408, 4410, 4430, 4431, 4432, 4434, 4441, 4442, 4443, 4445, 4448, 4449, 4450, 4452, 4469, 4470, 4471, 4473, 4484, 4485, 4486, 4488, 4501, 4502, 4503, 4505, 4520, 4521, 4522, 4524, 4542, 4543, 4544, 4546, 4558, 4559, 4560, 4570, 4572, 4573, 4574, 4576, 4586, 4587, 4588, 4590, 4596, 4597, 4598, 4600, 4615, 4616, 4617, 4619, 4629, 4630, 4631, 4633, 4641, 4642, 4643, 4645, 4653, 4654, 4655, 4657, 4675, 4676, 4677, 4679, 4686, 4687, 4688, 4690, 4708, 4709, 4710, 4720, 4722, 4723, 4724, 4726, 4736, 4737, 4738, 4740, 4753, 4754, 4755, 4757, 4760, 4761, 4762, 4764, 4777, 4778, 4779, 4781, 4791, 4792, 4793, 4795, 4810, 4811, 4812, 4814, 4826, 4827, 4828, 4830, 4845, 4846, 4847, 4849, 4859, 4860, 4861, 4863, 4873, 4874, 4875, 4877, 4885, 4886, 4887, 4889, 4909, 4910, 4911, 4913, 4920, 4921, 4922, 4924, 4927, 4928, 4929, 4931, 4948, 4949, 4950, 4952, 4959, 4960, 4961, 4963, 4966, 4967, 4968, 4970, 4977, 4978, 4979, 4981, 4997, 4998, 4999, 5001, 5014, 5015, 5016, 5018, 5026, 5027, 5028, 5030, 5047, 5048, 5049, 5051, 5071, 5072, 5073, 5075, 5081, 5082, 5083, 5085, 5096, 5097, 5098, 5100, 5103, 5104, 5105, 5107, 5118, 5119, 5120, 5122, 5128, 5129, 5130, 5132, 5149, 5150, 5151, 5153, 5173, 5174, 5175, 5177, 5183, 5184, 5185, 5187, 5193, 5194, 5195, 5197, 5213, 5214, 5215, 5217, 5234, 5235, 5236, 5238, 5250, 5251, 5252, 5254, 5265, 5266, 5267, 5269, 5284, 5285, 5286, 5288, 5303, 5304, 5305, 5307, 5327, 5328, 5329, 5331, 5334, 5335, 5336, 5338, 5358, 5359, 5360, 5370, 5372, 5373, 5374, 5376, 5394, 5395, 5396, 5398, 5409, 5410, 5411, 5413, 5421, 5422, 5423, 5425, 5443, 5444, 5445, 5447, 5459, 5460, 5461, 5463, 5483, 5484, 5485, 5487, 5507, 5508, 5509, 5511, 5521, 5522, 5523, 5525, 5532, 5533, 5534, 5536, 5546, 5547, 5548, 5550, 5557, 5558, 5559, 5561, 5576, 5577, 5578, 5580, 5592, 5593, 5594, 5596, 5603, 5604, 5605, 5607, 5620, 5621, 5622, 5624, 5630, 5631, 5632, 5634, 5641, 5642, 5643, 5645, 5652, 5653, 5654, 5656, 5666, 5667, 5668, 5670, 5681, 5682, 5683, 5685, 5701, 5702, 5703, 5705, 5720, 5721, 5722, 5724, 5727, 5728, 5729, 5731, 5744, 5745, 5746, 5748, 5768, 5769, 5770, 5772, 5780, 5781, 5782, 5784, 5804, 5805, 5806, 5816, 5818, 5819, 5820, 5822, 5830, 5831, 5832, 5834, 5837, 5838, 5839, 5841, 5847, 5848, 5849, 5851, 5869, 5870, 5871, 5873, 5888, 5889, 5890, 5892, 5903, 5904, 5905, 5907, 5924, 5925, 5926, 5928, 5944, 5945, 5946, 5948, 5963, 5964, 5963, 5964, 6010, 6011 };
    Array<int> grapheneParticlesToRestrain{ 0, 38, 641, 679 };
};
